package com.example.moham.losti.utilities;

import com.example.moham.losti.BuildConfig;

/**
 * Created by Farahat on 22-Jan-18.
 */

public class Constants {

    public static final String FIREBASE_URL = BuildConfig.UNIQUE_FIREBASE_DATABASE_ROOT_URL;
    public static final String FIREBASE_URL_DATA_TEST = BuildConfig.UNIQUE_FIREBASE_DATABASE_ROOT_URL + "test";


}
