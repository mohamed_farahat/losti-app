package com.example.moham.losti.model;

/**
 * Created by moham on 14-Jan-18.
 */

public class Post {
    int image;
    String owner, desc, time, reward, location;

    public Post() {
    }

    public Post(int image, String owner, String desc, String time, String reward, String location) {
        this.image = image;
        this.owner = owner;
        this.desc = desc;
        this.time = time;
        this.reward = reward;
        this.location = location;
    }

    public int getImage() {
        return image;
    }

    public String getOwner() {
        return owner;
    }

    public String getDesc() {
        return desc;
    }

    public String getTime() {
        return time;
    }

    public String getReward() {
        return reward;
    }

    public String getLocation() {
        return location;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}