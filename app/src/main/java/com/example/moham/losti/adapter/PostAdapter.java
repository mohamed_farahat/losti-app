package com.example.moham.losti.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.moham.losti.MainActivity;
import com.example.moham.losti.R;
import com.example.moham.losti.model.Post;

import java.util.List;

/**
 * Created by moham on 14-Jan-18.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.MyViewHolder> {
    Context context;

    boolean showingFirst = true;
    private List<Post> PostsList;


    ImageView NormalImageView;
    Bitmap ImageBit;
    float ImageRadius = 40.0f;


    public class MyViewHolder extends RecyclerView.ViewHolder {


        ImageView image;
        TextView owner;
        TextView desc;
        TextView reward;
        TextView time;
        TextView location;


        public MyViewHolder(View view) {
            super(view);

            image = (ImageView) view.findViewById(R.id.image);
            desc = (TextView) view.findViewById(R.id.desc);
            owner = (TextView) view.findViewById(R.id.owner);
            reward = (TextView) view.findViewById(R.id.price);
            time = (TextView) view.findViewById(R.id.time);
            location = (TextView) view.findViewById(R.id.location);

        }


    }


    public PostAdapter(Context context, List<Post> postsList) {
        this.context = context;
        PostsList = postsList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lost_item, parent, false);


        return new MyViewHolder(itemView);


    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Post post = PostsList.get(position);
        holder.image.setImageResource(post.getImage());
        holder.owner.setText(post.getOwner());
        holder.desc.setText((post.getDesc()));
        holder.time.setText(post.getTime());
        holder.reward.setText(post.getReward());
        holder.location.setText(post.getLocation());


    }

    @Override
    public int getItemCount() {
        return PostsList.size();
    }
}


