package com.example.moham.losti;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.example.moham.losti.NavActivities.HomeFragment;
import com.example.moham.losti.NavActivities.ProfileFragment;
import com.example.moham.losti.NavActivities.SettingsFragment;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends AppCompatActivity {

    private BottomBar bottomBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomBar = (BottomBar) findViewById(R.id.bottomBar);

        //bottom Selection
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
//                if (tabId == R.id.tab_favorites) {
//
//                }

                switch (tabId) {
                    case R.id.tab_profile:
                        FragmentManager profileManager = getSupportFragmentManager();
                        profileManager.beginTransaction().replace(R.id.myScrollingContent, new ProfileFragment()).commit();
                        break;

                    case R.id.tab_home:
                        FragmentManager homeManager = getSupportFragmentManager();
                        homeManager.beginTransaction().replace(R.id.myScrollingContent, new HomeFragment()).commit();
                        break;

                    case R.id.tab_settings:
                        FragmentManager settingsManager = getSupportFragmentManager();
                        settingsManager.beginTransaction().replace(R.id.myScrollingContent, new SettingsFragment()).commit();
                        break;
                }
            }
        });

        //bottom ReSelection
//        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
//            @Override
//            public void onTabReSelected(@IdRes int tabId) {
//                if (tabId == R.id.tab_favorites) {
//                    // The tab with id R.id.tab_favorites was reselected,
//                    // change your content accordingly.
//                }
//            }
//        });
    }

}
