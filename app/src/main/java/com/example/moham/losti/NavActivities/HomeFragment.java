package com.example.moham.losti.NavActivities;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.moham.losti.MainActivity;
import com.example.moham.losti.R;
import com.example.moham.losti.adapter.PostAdapter;
import com.example.moham.losti.model.Post;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    MainActivity mainActivity;

    private Integer[] IMAGE = {R.drawable.ic_commercial_air_company, R.drawable.ic_commercial_air_company, R.drawable.ic_commercial_air_company, R.drawable.ic_commercial_air_company, R.drawable.ic_commercial_air_company,
            R.drawable.ic_commercial_air_company, R.drawable.ic_commercial_air_company, R.drawable.ic_commercial_air_company};

    private String[] DESC = {"Wallet", "Mobile", "Watch", "Pin", "Cap", "Book", "personal id", "phone"};

    private String[] OWNER = {"Mohamed", "Ibrahem", "Mostafa", "Farahat", "HAidy", "Ahmed", "Eyad", "Esam"};

    private String[] LOCATION = {"Tanta", "Giza", "Heliopilis", "madin nasr", "Elrehab",
            "Heliopilis", "madin nasr", "Elrehab",};

    private String[] TIME = {"6h 21m ", "8h 11m ", "4h 40m ", "1h 30m ",
            "7h 21m ", "4h 11m - 1 stop , DXB", "9h 45m ", "12h 21m "};

    private String[] REWARD = {"21,685 USD", "45,421 USD", "22,500 USD", "10,450 USD", "32,752 USD", "60,625 USD", "50,875 USD", "55,452 USD"};

    private ArrayList<Post> LostPost;

    private RecyclerView recyclerView;
    private PostAdapter mAdapter;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);


        recyclerView = (RecyclerView) view.findViewById(R.id.myrec);
        LostPost = new ArrayList<>();


        for (int i = 0; i < IMAGE.length; i++) {
            Post beanClassForRecyclerView_contacts = new Post(IMAGE[i], DESC[i], OWNER[i], LOCATION[i], TIME[i],
                    REWARD[i]);

            LostPost.add(beanClassForRecyclerView_contacts);
        }


        mAdapter = new PostAdapter(getActivity(), LostPost);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        return view;

    }
}

