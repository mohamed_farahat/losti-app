package com.example.moham.losti.SignUp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.example.moham.losti.R;


public class SignupActivity extends AppCompatActivity {


//    FirebaseDatabase database;
//    DatabaseReference myRef ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

//        database = FirebaseDatabase.getInstance();
//        myRef = database.getReferenceFromUrl("https://losti-b3d1d.firebaseio.com/");
//
//        myRef.setValue("Hello, World!");
    }

    public void onSignInClicked(View view) {
        Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void onCreateAccountClicked(View view) {
        Intent intent = new Intent(SignupActivity.this, RegistrationActivity.class);
        startActivity(intent);
    }
}
